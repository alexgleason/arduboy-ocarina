# Ocarina

A tiny Arduboy sketch that maps the buttons to sounds in a similar manner to the instruments in The Legend of Zelda N64 titles.

**License:** GPL-3.0+

![Arduboy running Ocarina](photo.jpg)
